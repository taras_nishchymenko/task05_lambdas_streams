package com.nishchymenko.task1;

public class Main {

  public static void main(String[] args) {
    IntOperation max = (a, b, c) -> {
      int temp = a > b ? a : b;
      return temp > c ? temp : c;
    };

    IntOperation avg = (a, b, c) -> {
      if (a < b && a > c || a > b && a < c) {
        return a;
      }
      if (b < a && b > c || b > a && b < c) {
        return b;
      }
      return c;
    };

    int a = 100;
    int b = 45;
    int c = 44;

    System.out.println(max.function(a, b, c));
    System.out.println(avg.function(a, b, c));
  }

}
