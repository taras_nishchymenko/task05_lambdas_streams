package com.nishchymenko.task1;

@FunctionalInterface
public interface IntOperation {
  int function(int arg1, int arg2, int arg3);
}
