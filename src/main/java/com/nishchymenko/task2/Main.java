package com.nishchymenko.task2;

public class Main {

  public static void main(String[] args) {
    CommandExecutor executor = new CommandExecutor();
    Command command1 = str -> {
      System.out.println("command 1 " + str);
    };
    Command command2 = new Command() {
      @Override
      public void invoke(String arg) {
        System.out.println("command 2 " + arg);
      }
    };
    Command command3 = System.out::println;
    Command command4 = new Command4();

    executor.executeCommands(command1, command2, command3, command4);

  }

}
