package com.nishchymenko.task2;

import java.time.LocalDateTime;

public class CommandExecutor {

  public void executeCommands(Command ... commands) {
    for (Command command : commands) {
      command.invoke(LocalDateTime.now().toString());
    }
  }

}
