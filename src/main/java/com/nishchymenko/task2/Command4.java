package com.nishchymenko.task2;

public class Command4 implements Command {

  @Override
  public void invoke(String arg) {
    System.out.println("command 4 " + arg);
  }
}
