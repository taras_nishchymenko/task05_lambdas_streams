package com.nishchymenko.task2;

@FunctionalInterface
public interface Command {

  void invoke(String arg);
}
